+++
class= "sponsors"
type = "sponsors"
image = "/img/partners/bytemark.png"
href = "https://www.bytemark.co.uk/"
+++

At Bytemark we believe that Linux and the Free Software ecosystem play a vital part in underpinning an open, co-operative and global internet. We are proud to give back and sponsor Manjaro to publishing free software using our network. Bytemark's datacentre uses fresh-air cooling, not common in the UK. Each of its servers is built using efficient power supplies as certified by the 80plus scheme, which requires power supplies to be at least 80% efficient at up to 100% rated load. Over the years, we’ve designed our own national network, built our own cloud hosting platform and now we’ve set our sights on developing a managed Kubernetes service. Our customer base has grown with us, spanning hobbyists to multi-million-pound businesses.

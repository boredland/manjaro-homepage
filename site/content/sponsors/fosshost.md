+++
class= "sponsors"
type = "sponsors"
image = "/img/partners/fosshost.png"
href = "https://fosshost.org/"
+++

We're on a mission to empower and support every free and open source software project. To go further, together. Our work never stops. Fosshost C.I.C. is a non-profit UK-registered Community Interest Company (CIC), Limited by Guarantee, without share capital. Launched in April 2020 to provide open-source projects and initiatives access to no-cost distributed cloud computing services. We are happy to sponsor one mirror for hosting the repository of Manjaro Linux, powered by fastly, to reach the Manjaro community with the best speeds possible. Also we provide Manjaro with an ARM build server, so packages of Manjaro ARM can be faster built and delivered.

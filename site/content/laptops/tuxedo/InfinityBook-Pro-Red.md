+++
class= "hardware"
model = "InfinityBook Pro Red"
price = "From 1,199.00 EUR"
brand = "Tuxedo"
type = "hardware"
image = "InfinityBook-Pro-Red"
buylink = "https://www.tuxedocomputers.com/en/Linux-Hardware/Linux-Notebooks/15-16-inch/Manjaro-InfinityBook-Pro-15.tuxedo#"
specs = "Intel® Core i5-10210U or i7-10510U, DDR4 SO-DIMM 8GB to 64GB, SSDs 250GB to 2000GB, 15.6\", Full-HD IPS-Panel, Battery up to 12H"
+++

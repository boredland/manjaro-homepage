+++
class= "hardware"
model = "InfinityBook S 14 v5"
price = "From 1.264,00 EUR"
brand = "Tuxedo"
intro = "TUXEDO Computers are individually built, assembled and installed in Germany and delivered in such a way that you only have to unpack, connect and switch them on!"
type = "hardware"
image = "InfinityBook-S14"
buylink = "https://www.tuxedocomputers.com/de/Linux-Hardware/Linux-Notebooks/10-14-Zoll/Manjaro-InfinityBook-S-14-v5_1.tuxedo"
specs = "Intel Core i5-10210U or i7-10510U, DDR4 2666 MHz 8 GB On board to 40 GB, 250GB to 2000GB, 14 Full-HD IPS-Panel, Manjaro super-key, Battery up to 24H"
+++


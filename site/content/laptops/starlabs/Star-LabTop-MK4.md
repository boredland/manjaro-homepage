+++
class= "hardware"
model = "Star LabTop MK4"
price = "From 699.00 GBP"
brand = "Starlabs"
type = "hardware"
image = "Star_LabTop_MK4"
buylink = "https://starlabs.systems/products/labtop-mk-iv?rfsn=3932868.5e88d65#"
specs = "Intel® Core® i3-10110u (2.1GHz dual-core) with Intel® UHD 620 graphics, 13.3\" IPS LED-backlit matte display, 16GB 2400MHz, 240GB SSD, Battery Up to 7H"
+++


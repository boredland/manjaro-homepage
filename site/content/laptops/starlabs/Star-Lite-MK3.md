+++
class= "hardware"
model = "Star Lite MK3"
price = "From 399.00 GBP"
brand = "Starlabs"
type = "hardware"
image = "starlite-mk2"
buylink = "https://starlabs.systems/pages/lite-mk-iii?rfsn=3932868.5e88d65#"
specs = "Intel® Pentium™ N5030 with Intel® HD 605 graphics, 8 GB 1600MHz, 240GB SSD, Display 11.6\" matte display with IPS technology, Battery Up to 7H"
+++


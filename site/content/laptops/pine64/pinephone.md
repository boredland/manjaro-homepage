+++
class= "hardware"
model = "PinePhone"
price = "149.00 USD"
brand = "Pine64"
type = "hardware"
image = "pinephone"
buylink = "https://pine64.com/product-category/pinephone/"
specs = "64-bit Quad-core 1.2 GHz ARM Cortex A-53, MALI-400MP2, 16GB eMMC, 3GB LPDDR3 SDRAM, micro SD Card support, 5MP back & 2MP front camera, Global LTE bands, Up to 11h battery life"
+++

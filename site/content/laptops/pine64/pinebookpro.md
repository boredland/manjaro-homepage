+++
class= "hardware"
model = "Pinebook Pro"
price = "199.99 USD"
brand = "Pine64"
type = "hardware"
image = "pinebook-pro"
buylink = "https://www.pine64.org/pinebook-pro/"
specs = "Rockchip RK3399 SOC with Mali T860 MP4 GPU, 4GB LPDDR4 RAM, 1080p IPS Panel, Magnesium Alloy Shell body, Bootable Micro SD Slot, 64GB of eMMC, USB-C (Data, Power and Video out), Lithium Polymer Battery (10000mAH,), WiFi 802.11 AC + Bluetooth 5.0, Front-Facing Camera (1080p)"
+++

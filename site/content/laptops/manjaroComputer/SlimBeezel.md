+++
class= "hardware"
model = "Manjaro AIO 23,8\" Slim Bezel"
price = "From 835.00 EUR"
brand = "Manjaro"
type = "hardware"
image = "aio23manvoor"
buylink = "https://manjarocomputer.eu/index.php/manjaro-aio-23-8-slim-bezel-fhd-computer.html"
specs = "Intel Core i3-9100, 23,8\" Slim Bezel FHD (1920 x 1080), ASUS® H310-T mainboard, 8GB RAM, Intel HD Graphics, 240GB SSD, 2 Channel High Definition Soundcard + MIC/Headphone Jack, Wireless/Wired Networking +BT 5.0"
+++

+++
class= "hardware"
model = "ManjaroBook 14\" AMD Ryzen"
price = "From 930.00 EUR"
brand = "Manjaro"
type = "hardware"
image = "14amdmanlinks"
buylink = "https://manjarocomputer.eu/index.php/manjarobook-14-amd-ryzen.html/"
specs = "AMD Ryzen™ 5 3500U CPU, Radeon™ Vega 8, 8GB SODIMM DDR4, 512GB M.2 SSD, Intel Wireless-AC + BT, QWERTY keyboard, 1.05 KG weight, Battery up to 10H"
+++


#!/bin/bash
set -euo pipefail
#set -x

# Get pacman and vercmp
if [[ ! -f pacman-static ]]; then
	curl "https://pkgbuild.com/~eschwartz/repo/x86_64/pacman-static-5.2.1-6-x86_64.pkg.tar.xz" | \
		bsdtar --strip-components=2 -xf - \*pacman-static \*vercmp-static
	chmod u+x pacman-static vercmp-static
fi

arch="${1:-x86_64}"
# Get repos for architecture
declare -a repos
while read -r repo; do
    repo="${repo:1:-1}"
    if [[ "$repo" != "options" ]]; then
        repos+=("$repo")
    fi
done< <(grep '^\[' "./pacman.${arch}.conf" 2>/dev/null)
[[ -z "${repos[*]}" ]] && exit 3   # bad architecture

[ -d "${arch}" ] || mkdir "${arch}"
cd "${arch}"

declare -a branches=(stable stable-staging testing unstable)
declare -r mirror="https://mirror.easyname.at/manjaro"
prearch=""
archreal="$arch"
if [[ "$arch" == "arm" ]]; then
    prearch="arm-"
    archreal="aarch64"
    branches=(stable testing unstable)
fi

# Refresh package lists
for branch in ${branches[@]}; do
	mkdir -p $branch/sync
	for repo in ${repos[@]}; do
		wget -qN $mirror/${prearch}$branch/$repo/${archreal}/$repo.db -P $branch/sync/
	done
done

convertName() {
    #old: sed "s|core |core#|"
    declare -a datas
    while read INPUT; do
        datas=( $INPUT )
        echo "${datas[1]}#${datas[0]} ${datas[2]}"
    done
}

if [[ -f ../packages.${arch}.txt ]]; then
    cp ../packages.${arch}.txt datas.work -f
fi

date="$(curl -s ${mirror}/${prearch}unstable/state | awk -F'=' '/^date/ {print $2}')"
lastupdate="${date:0:-4}"
stagingexists=0
if $([[ " ${branches[@]} " =~ " stable-staging " ]]); then
    stagingexists=1
fi
cat > "datas.work" << EOH
{
  "lastupdate": "${lastupdate//T/ }",
  "stable_staging": $stagingexists,
  "packages": [
EOH
comma=""
# Process each repo in turn
for repo in ${repos[@]}; do
    if [[ $stagingexists == 1 ]]; then
        #echo "stable-staging exists"
        mapfile -t map < <(join -a1 -a2 -e "n/a" -o auto --nocheck-order <(join -a1 -a2 -e "n/a" -o auto --nocheck-order <(join -a1 -a2 -e "n/a" -o auto --nocheck-order \
                <(../pacman-static --config "../pacman.${arch}.conf" -Sl -b stable "$repo" | convertName | sort) \
                <(../pacman-static --config "../pacman.${arch}.conf" -Sl -b testing "$repo" | convertName | sort)) \
                <(../pacman-static --config "../pacman.${arch}.conf" -Sl -b unstable "$repo" | convertName| sort)) \
                <(../pacman-static --config "../pacman.${arch}.conf" -Sl -b stable-staging "$repo" | convertName | sort))
    else
        #echo "stable-staging NOT exists"
        mapfile -t map < <(join -a1 -a2 -e "n/a" -o auto --nocheck-order <(join -a1 -a2 -e "n/a" -o auto --nocheck-order \
            <(../pacman-static --config "../pacman.${arch}.conf" -Sl -b stable "$repo" | convertName | sort) \
            <(../pacman-static --config "../pacman.${arch}.conf" -Sl -b testing "$repo" | convertName | sort)) \
            <(../pacman-static --config "../pacman.${arch}.conf" -Sl -b unstable "$repo" | convertName | sort))
    fi

    none="n/a"
	for package in "${map[@]}"; do
		IFS=$' ' read -r name stable testing unstable stablestaging<<< "$package"
		[[ "$testing" == "" ]] && testing=none
		[[ "$stable" == "" ]] && stable=none
		[[ "$unstable" == "" ]] && unstable=none
		stagingitem=""
		if [[ $stagingexists == 1 ]]; then
            [[ "$stablestaging" == "" ]] && stablestaging=none
            stagingitem=",\"stable_staging\":\"$stablestaging\""
		fi
		echo "${comma}[\"$name\", {\"stable\":\"$stable\",\"testing\":\"$testing\",\"unstable\":\"$unstable\"$stagingitem,\"repository\":\"$repo\"}]" >> "datas.work"
		[[ -z "$comma" ]] && comma=","
	done
done

echo -e "]}" >> "datas.work"

cp datas.work ../packages.${arch}.json -f


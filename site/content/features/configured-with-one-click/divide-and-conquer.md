+++
title = "Divide and conquer"
image = "manjaro_useraccounts"
type = "configured-with-one-click"
+++ 
If multiple people use your computer, just create an account for each of them. Each user has their personal storage and appropriate permissions on the system individually.

+++
class= "partners"
type = "partners"
image = "/img/partners/beelink.png"
href = "http://www.bee-link.com/"
+++

The 'Beelink' brand is a registered trademark since 2013 and is registered in 28 countries around the world. Beelink is a prominent brand in Computer hardware. They manufacture Mini Pc and Android TV boxes.  They're one of the enormous manufacturers of these type of devices. The devices manufactured by them are of high quality. Beelink is more than just a hardware manufacturer,  they have their own hardware, software design and R&D team, they provide ODM services to other brands too. Beelink is happy to partner with Manjaro and will be shipping our top of the line Mini Pc with Manjaro Linux pre-installed.

+++
class= "partners"
type = "partners"
image = "/img/partners/shells-com.png"
href = "https://shells.com/manjaro"
+++

At Shells, we are closing the digital divide by providing simple, secure, and affordable access to a powerful, future-proof computing solution that's accessible on any device.  We believe in simplicity and flexibility for our users which is why we are so well aligned with Manjaro.  Through our collaboration, we are excited to open new doors for users who want to take control of their computer through Manjaro.

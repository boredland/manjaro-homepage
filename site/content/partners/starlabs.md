+++
class= "partners"
type = "partners"
image = "/img/partners/starlabs.png"
href = "https://starlabs.systems/?rfsn=3932868.5e88d65#"
+++

Our partnership with Manjaro is beneficial not to just us, but our users as well. With our aligned interest of making open-source software more accessible, the combination of Manjaro and Star Labs hardware creates an unheard-of experience - seamless access to cutting-edge hardware and software. Manjaro is eager and dedicated to continually improving and adding more features, and continue to excel with the delivery of these. We've worked together and will continue to do so, for many hardware related aspects.

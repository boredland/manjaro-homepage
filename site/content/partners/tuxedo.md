+++
class= "partners"
type = "partners"
image = "/img/partners/tuxedo.svg"
href = "https://www.tuxedocomputers.com/"
+++

At TUXEDO Computers you don't only get individually configurable Linux No‍te‍books und PCs, but also hardware that is perfectly optimized for running Linux! Besides the technical aspects, the work of the community is the most important aspect. In Manjaro TUXEDO Computers has found a partner that is both professional and committed. This partnership and with it the work of the Manjaro Community shall be supported. Therefore TUXEDO Computers is directly involved in the work of the Manjaro community, supporting developers with own resources.

+++
Download_x64 = "https://github.com/manjaro-arm/rpi4-images/releases/download/21.07/Manjaro-ARM-gnome-rpi4-21.07.img.xz"
Download_x64_Checksum = "af4c45810d87714b599413fd813dd7085abf4723"
Download_x64_Sig = ""
Download_x64_Torrent = "https://github.com/manjaro-arm/rpi4-images/releases/download/21.07/Manjaro-ARM-gnome-rpi4-21.07.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Raspberry Pi 4 Gnome"
Screenshot = "arm-gnome-full.png"
Youtube = ""
edition = "ARM"
device = "Raspberry Pi 4"
shortDescription = "Manjaro Gnome on ARM is a modern DE with a modern workflow."
Thumbnail = "arm-gnome-full.png"
Version = "21.07"
date = "07.2021"
title = "Raspberry Pi 4 Gnome"
type="download-edition"
weight = "1"
meta_description = "Manjaro Gnome on ARM is a modern DE with a modern workflow."
meta_keywords = "manjaro gnome arm, manjaro download"
+++

For people who look for a modern user experience.

This edition comes with Gnome, a desktop based on GTK3 and GTK4, that has a slick user experience.

Device Maintainer: Ray Sherwin
Edition Maintainer: Andreas Gerlach

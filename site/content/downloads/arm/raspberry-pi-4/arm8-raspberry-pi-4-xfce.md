+++
Download_x64 = "https://github.com/manjaro-arm/rpi4-images/releases/download/21.07/Manjaro-ARM-xfce-rpi4-21.07.img.xz"
Download_x64_Checksum = "48a40dca7a5f901c3d7f61a6c3cb3c6baa6aefa4"
Download_x64_Sig = ""
Download_x64_Torrent = "https://github.com/manjaro-arm/rpi4-images/releases/download/21.07/Manjaro-ARM-xfce-rpi4-21.07.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Raspberry Pi 4 XFCE"
Screenshot = "arm-xfce-full.png"
Youtube = ""
edition = "ARM"
device = "Raspberry Pi 4"
shortDescription = "XFCE on ARM is one of the fastest DE's available and the most stable."
Thumbnail = "arm-xfce-full.png"
Version = "21.07"
date = "07.2021"
title = "Raspberry Pi 4 XFCE"
type="download-edition"
weight = "1"
meta_description = "Manjaro XFCE on ARM is one of the fastest DE's available and the most stable."
meta_keywords = "manjaro xfce arm, manjaro download"
+++

This edition is supported by the Manjaro ARM team and comes with XFCE desktop.

XFCE is a lightweight, and very stable, GTK based desktop. It's modular and very customizable.

Device Maintainer: Ray Sherwin
Edition Maintainer: Ray Sherwin

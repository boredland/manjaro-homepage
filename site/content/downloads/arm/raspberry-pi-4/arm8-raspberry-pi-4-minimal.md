+++
Download_x64 = "https://github.com/manjaro-arm/rpi4-images/releases/download/21.07/Manjaro-ARM-minimal-rpi4-21.07.img.xz"
Download_x64_Checksum = "87214e7df25463ce28e0409f3f70b5f8ff8347d2"
Download_x64_Sig = ""
Download_x64_Torrent = "https://github.com/manjaro-arm/rpi4-images/releases/download/21.07/Manjaro-ARM-minimal-rpi4-21.07.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Raspberry Pi 4 Minimal"
Screenshot = "arm-minimal-full.png"
Youtube = ""
edition = "ARM"
device = "Raspberry Pi 4"
shortDescription = "For people that do not need a desktop"
Thumbnail = "arm-minimal-full.png"
Version = "21.07"
date = "07.2021"
title = "Raspberry Pi 4 Minimal"
type="download-edition"
weight = "1"
meta_description = "Manjaro minimal ARM for people that do not need a desktop"
meta_keywords = "manjaro minimal arm, manjaro download"
+++

This edition is supported by the Manjaro ARM team.

The Minimal edition is Manjaro ARM, but without a Desktop Environment or Xorg at all.

Device Maintainer: Ray Sherwin
Edition Maintainer: Dan Johansen

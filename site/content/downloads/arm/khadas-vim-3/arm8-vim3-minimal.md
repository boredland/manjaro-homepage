+++
Download_x64 = "https://github.com/manjaro-arm/vim3-images/releases/download/21.07/Manjaro-ARM-minimal-vim3-21.07.img.xz"
Download_x64_Checksum = "556ca45d77a573c49b4bf86f1d218f3db3d28cba"
Download_x64_Sig = ""
Download_x64_Torrent = "https://github.com/manjaro-arm/vim3-images/releases/download/21.07/Manjaro-ARM-minimal-vim3-21.07.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Khadas Vim 3 Minimal"
Screenshot = "arm-minimal-full.png"
Youtube = ""
edition = "ARM"
device = "Khadas Vim 3"
shortDescription = "For people that do not need a desktop"
Thumbnail = "arm-minimal-full.png"
Version = "21.07"
date = "07.2021"
title = "Khadas Vim 3 Minimal"
type="download-edition"
weight = "1"
meta_description = "Manjaro arm for people that do not need a desktop"
meta_keywords = "manjaro minimal arm, manjaro download"
+++

The Minimal edition is Manjaro ARM, but without a Desktop Environment or Xorg at all.

Device Maintainer: Furkan Kardame
Edition Maintainer: Dan Johansen

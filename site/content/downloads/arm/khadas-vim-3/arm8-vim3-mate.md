+++
Download_x64 = "https://github.com/manjaro-arm/vim3-images/releases/download/21.07/Manjaro-ARM-mate-vim3-21.07.img.xz"
Download_x64_Checksum = "ff1c5bc20ea876cf56beb50d9989b5128facc83f"
Download_x64_Sig = ""
Download_x64_Torrent = "https://github.com/manjaro-arm/vim3-images/releases/download/21.07/Manjaro-ARM-mate-vim3-21.07.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Khadas Vim 3 MATE"
Screenshot = "arm-mate-full.png"
Youtube = ""
edition = "ARM"
device = "Khadas Vim 3"
shortDescription = "Manjaro MATE on ARM is a modern looking DE that follows the Gnome2 paradigm."
Thumbnail = "arm-mate-full.png"
Version = "21.07"
date = "07.2021"
title = "Khadas Vim 3 MATE"
type="download-edition"
weight = "1"
meta_description = "Manjaro MATE on ARM is a modern looking DE that follows the Gnome2 paradigm."
meta_keywords = "manjaro mate arm, manjaro download"
+++

For people who look for a traditional experience

This edition comes with Mate, a desktop environment that continues the legacy of traditional user experience while carefully improving and modernizing it when needed.

Device Maintainer: Furkan Kardame
Edition Maintainer: Ray Sherwin

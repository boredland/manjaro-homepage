+++
Download_x64 = "https://github.com/manjaro-arm/vim3-images/releases/download/21.07/Manjaro-ARM-gnome-vim3-21.07.img.xz"
Download_x64_Checksum = "8cf5e654c4a79233b8fbd0f549a4123dd987e555"
Download_x64_Sig = ""
Download_x64_Torrent = "https://github.com/manjaro-arm/vim3-images/releases/download/21.07/Manjaro-ARM-gnome-vim3-21.07.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Khadas Vim 3 Gnome"
Screenshot = "arm-gnome-full.png"
Youtube = ""
edition = "ARM"
device = "Khadas Vim 3"
shortDescription = "Manjaro Gnome on ARM is a modern DE with a modern workflow."
Thumbnail = "arm-gnome-full.png"
Version = "21.07"
date = "07.2021"
title = "Khadas Vim 3 Gnome"
type="download-edition"
weight = "1"
meta_description = "Manjaro Gnome on ARM is a modern DE with a modern workflow."
meta_keywords = "manjaro gnome arm, manjaro download"
+++

For people who look for a modern user experience.

This edition comes with Gnome, a desktop based on GTK3 and GTK4, that has a slick user experience.

Device Maintainer: Furkan Kardame
Edition Maintainer: Andreas Gerlach

+++
Download_x64 = "https://github.com/manjaro-arm/vim3-images/releases/download/21.07/Manjaro-ARM-xfce-vim3-21.07.img.xz"
Download_x64_Checksum = "635003112c4fa3ab1f356b4568f475d77d352509"
Download_x64_Sig = ""
Download_x64_Torrent = "https://github.com/manjaro-arm/vim3-images/releases/download/21.07/Manjaro-ARM-xfce-vim3-21.07.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Khadas Vim 3 XFCE"
Screenshot = "arm-xfce-full.png"
Youtube = ""
edition = "ARM"
device = "Khadas Vim 3"
shortDescription = "XFCE on ARM is one of the fastest DE's available and the most stable."
Thumbnail = "arm-xfce-full.png"
Version = "21.07"
date = "07.2021"
title = "Khadas Vim 3 XFCE"
type="download-edition"
weight = "1"
meta_description = "Manjaro XFCE on ARM is one of the fastest DE's available and the most stable."
meta_keywords = "manjaro xfce arm, manjaro download"
+++

This edition is supported by the Manjaro ARM team and comes with XFCE desktop.

XFCE is a lightweight, and very stable, GTK based desktop. It's modular and very customizable.

Device Maintainer: Furkan Kardame
Edition Maintainer: Ray Sherwin

+++
Download_x64 = "https://github.com/manjaro-arm/vim3-images/releases/download/21.07/Manjaro-ARM-sway-vim3-21.07.img.xz"
Download_x64_Checksum = "2e7c8a0bb72cdedb1bc582b0ff135d370dbe7231"
Download_x64_Sig = ""
Download_x64_Torrent = "https://github.com/manjaro-arm/vim3-images/releases/download/21.07/Manjaro-ARM-sway-vim3-21.07.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Khadas Vim 3 Sway"
Screenshot = "arm-sway-full.png"
Youtube = ""
edition = "ARM"
device = "Khadas Vim 3"
shortDescription = "Manjaro ARM with the lightweight tiling window manager sway."
Thumbnail = "arm-sway-full.png"
Version = "21.07"
date = "07.2021"
title = "Khadas Vim 3 Sway"
type="download-edition"
weight = "1"
meta_description = "Manjaro sway on ARM is a lightweight tiling window manager for Wayland"
meta_keywords = "manjaro sway arm, manjaro download"
+++

This edition is supported by the Manjaro ARM team and comes with the sway tiling window manager.

Sway is an extremely lightweight tiling window manager for Wayland, famous for its efficiency with screen space and keyboard controlled workflow. The ideal environment for text and terminal focused usage for advanced users.

Device Maintainer: Furkan Kardame
Edition Maintainer: Andreas Gerlach

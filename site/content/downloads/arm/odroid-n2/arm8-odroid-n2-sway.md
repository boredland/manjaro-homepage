+++
Download_x64 = "https://github.com/manjaro-arm/on2-images/releases/download/21.06/Manjaro-ARM-sway-on2-21.06.img.xz"
Download_x64_Checksum = "e0efe5515d7fc02211469869605cb9e119193b3c"
Download_x64_Sig = "https://github.com/manjaro-arm/on2-images/releases/download/21.06/Manjaro-ARM-sway-on2-21.06.img.xz.sig"
Download_x64_Torrent = "https://github.com/manjaro-arm/on2-images/releases/download/21.06/Manjaro-ARM-sway-on2-21.06.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Odroid N2 Sway"
Screenshot = "arm-sway-full.png"
Youtube = ""
edition = "ARM"
device = "Odroid N2"
shortDescription = "Manjaro ARM with the lightweight tiling window manager sway."
Thumbnail = "arm-sway-full.png"
Version = "21.06"
date = "06.2021"
title = "Odroid N2 Sway"
type="download-edition"
weight = "1"
meta_description = "Manjaro sway on ARM is a lightweight tiling window manager for Wayland"
meta_keywords = "manjaro sway arm, manjaro download"
+++

This edition is supported by the Manjaro ARM team and comes with the sway tiling window manager.

Sway is an extremely lightweight tiling window manager for Wayland, famous for its efficiency with screen space and keyboard controlled workflow. The ideal environment for text and terminal focused usage for advanced users.

Device Maintainer: Dan Johansen
Edition Maintainer: Andreas Gerlach

+++
Download_x64 = "https://github.com/manjaro-arm/rock64-images/releases/download/21.06/Manjaro-ARM-minimal-rock64-21.06.img.xz"
Download_x64_Checksum = "0362ba585c776387809b51410ebae814504da02c"
Download_x64_Sig = "https://github.com/manjaro-arm/rock64-images/releases/download/21.06/Manjaro-ARM-minimal-rock64-21.06.img.xz.sig"
Download_x64_Torrent = "https://github.com/manjaro-arm/rock64-images/releases/download/21.06/Manjaro-ARM-minimal-rock64-21.06.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Rock64 Minimal"
Screenshot = "arm-minimal-full.png"
Youtube = ""
edition = "ARM"
device = "Rock64"
shortDescription = "For people that do not need a desktop"
Thumbnail = "arm-minimal-full.png"
Version = "21.06"
date = "06.2021"
title = "Rock64 Minimal"
type="download-edition"
weight = "1"
meta_description = "Manjaro minimal ARM for people that do not need a desktop"
meta_keywords = "manjaro minimal arm, manjaro download"
+++

The Minimal edition is Manjaro ARM, but without a Desktop Environment or Xorg at all.

Device Maintainer: Dan Johansen
Edition Maintainer: Dan Johansen

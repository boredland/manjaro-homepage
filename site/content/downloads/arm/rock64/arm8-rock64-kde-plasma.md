+++
Download_x64 = "https://github.com/manjaro-arm/rock64-images/releases/download/21.06/Manjaro-ARM-kde-plasma-rock64-21.06.img.xz"
Download_x64_Checksum = "d90ae4a2ed0e14a470f73892b137bdd7ad59ce21"
Download_x64_Sig = "https://github.com/manjaro-arm/rock64-images/releases/download/21.06/Manjaro-ARM-kde-plasma-rock64-21.06.img.xz.sig"
Download_x64_Torrent = "https://github.com/manjaro-arm/rock64-images/releases/download/21.06/Manjaro-ARM-kde-plasma-rock64-21.06.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Rock64 KDE Plasma"
Screenshot = "arm-kde-full.jpg"
Youtube = ""
edition = "ARM"
device = "Rock64"
shortDescription = "Plasma on ARM is a full-featured desktop experience and all the bells and whistles you might want."
Thumbnail = "arm-kde-full.png"
Version = "21.06"
date = "06.2021"
title = "Rock64 KDE Plasma"
type="download-edition"
weight = "1"
meta_description = "Manjaro Plasma on ARM is a full-featured desktop experience and all the bells and whistles you might want."
meta_keywords = "manjaro plasma arm, manjaro download"
+++

This edition is supported by the Manjaro ARM team and comes with Plasma desktop.

KDE Plasma is for people who want a user-friendly and customizable desktop. It is a feature-rich and versatile desktop environment that provides several different styles of menu to access applications. An excellent built-in interface to easily access and install new themes, widgets, etc, from the internet is also worth mentioning. KDE Plasma is simple by default, a clean work area for real-world usage which intends to stay out of your way, so the user is enabled to create the workflow that makes it more effective to complete tasks.

Device Maintainer: Dan Johansen
Edition Maintainer: Dan Johansen

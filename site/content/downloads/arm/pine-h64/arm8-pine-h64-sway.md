+++
Download_x64 = "https://github.com/manjaro-arm/pine-h64-images/releases/download/21.06/Manjaro-ARM-sway-pine-h64-21.06.img.xz"
Download_x64_Checksum = "247f7b03bccc241b9fb00c1d248172513e6570df"
Download_x64_Sig = "https://github.com/manjaro-arm/pine-h64-images/releases/download/21.06/Manjaro-ARM-sway-pine-h64-21.06.img.xz.sig"
Download_x64_Torrent = "https://github.com/manjaro-arm/pine-h64-images/releases/download/21.06/Manjaro-ARM-sway-pine-h64-21.06.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Pine H64 Sway"
Screenshot = "arm-sway-full.png"
Youtube = ""
edition = "ARM"
device = "Pine H64"
shortDescription = "Manjaro ARM with the lightweight tiling window manager sway."
Thumbnail = "arm-sway-full.png"
Version = "21.06"
date = "06.2021"
title = "Pine H64 Sway"
type="download-edition"
weight = "1"
meta_description = "Manjaro sway on ARM is a lightweight tiling window manager for Wayland"
meta_keywords = "manjaro sway arm, manjaro download"
+++

This edition is supported by the Manjaro ARM team and comes with the sway tiling window manager.

Sway is an extremely lightweight tiling window manager for Wayland, famous for its efficiency with screen space and keyboard controlled workflow. The ideal environment for text and terminal focused usage for advanced users.

Device Maintainer: Dan Johansen
Edition Maintainer: Andreas Gerlach

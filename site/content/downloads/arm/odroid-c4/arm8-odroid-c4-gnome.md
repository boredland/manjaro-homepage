+++
Download_x64 = "https://github.com/manjaro-arm/oc4-images/releases/download/21.06/Manjaro-ARM-gnome-oc4-21.06.img.xz"
Download_x64_Checksum = "bb98c91a7c318309ab752d332146640fa3b6bc2d"
Download_x64_Sig = "https://github.com/manjaro-arm/oc4-images/releases/download/21.06/Manjaro-ARM-gnome-oc4-21.06.img.xz.sig"
Download_x64_Torrent = "https://github.com/manjaro-arm/oc4-images/releases/download/21.06/Manjaro-ARM-gnome-oc4-21.06.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Odroid C4 Gnome"
Screenshot = "arm-gnome-full.png"
Youtube = ""
edition = "ARM"
device = "Odroid C4"
shortDescription = "Manjaro Gnome on ARM is a modern DE with a modern workflow."
Thumbnail = "arm-gnome-full.png"
Version = "21.06"
date = "06.2021"
title = "Odroid C4 Gnome"
type="download-edition"
weight = "1"
meta_description = "Manjaro Gnome on ARM is a modern DE with a modern workflow."
meta_keywords = "manjaro gnome arm, manjaro download"
+++

For people who look for a modern user experience.

This edition comes with Gnome, a desktop based on GTK3 and GTK4, that has a slick user experience.

Device Maintainer: Dan Johansen
Edition Maintainer: Andreas Gerlach

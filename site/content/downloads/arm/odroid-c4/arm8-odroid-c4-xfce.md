+++
Download_x64 = "https://github.com/manjaro-arm/oc4-images/releases/download/21.06/Manjaro-ARM-xfce-oc4-21.06.img.xz"
Download_x64_Checksum = "30dee67b146661158d295db1834a451e0c45c6d2"
Download_x64_Sig = "https://github.com/manjaro-arm/oc4-images/releases/download/21.06/Manjaro-ARM-xfce-oc4-21.06.img.xz.sig"
Download_x64_Torrent = "https://github.com/manjaro-arm/oc4-images/releases/download/21.06/Manjaro-ARM-xfce-oc4-21.06.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Odroid C4 XFCE"
Screenshot = "arm-xfce-full.png"
Youtube = ""
edition = "ARM"
device = "Odroid C4"
shortDescription = "XFCE on ARM is one of the fastest DE's available and the most stable."
Thumbnail = "arm-xfce-full.png"
Version = "21.06"
date = "06.2021"
title = "Odroid C4 XFCE"
type="download-edition"
weight = "1"
meta_description = "Manjaro XFCE on ARM is one of the fastest DE's available and the most stable."
meta_keywords = "manjaro xfce arm, manjaro download"
+++

This edition is supported by the Manjaro ARM team and comes with XFCE desktop.

XFCE is a lightweight, and very stable, GTK based desktop. It's modular and very customizable.

Device Maintainer: Dan Johansen
Edition Maintainer: Ray Sherwin

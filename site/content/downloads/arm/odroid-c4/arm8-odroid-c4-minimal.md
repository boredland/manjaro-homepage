+++
Download_x64 = "https://github.com/manjaro-arm/oc4-images/releases/download/21.06/Manjaro-ARM-minimal-oc4-21.06.img.xz"
Download_x64_Checksum = "b672ed2ccf26152b4c0d35c7c3a88d411e874c32"
Download_x64_Sig = "https://github.com/manjaro-arm/oc4-images/releases/download/21.06/Manjaro-ARM-minimal-oc4-21.06.img.xz.sig"
Download_x64_Torrent = "https://github.com/manjaro-arm/oc4-images/releases/download/21.06/Manjaro-ARM-minimal-oc4-21.06.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Odroid C4 Minimal"
Screenshot = "arm-minimal-full.png"
Youtube = ""
edition = "ARM"
device = "Odroid C4"
shortDescription = "For people that do not need a desktop"
Thumbnail = "arm-minimal-full.png"
Version = "21.06"
date = "06.2021"
title = "Odroid C4 Minimal"
type="download-edition"
weight = "1"
meta_description = "Manjaro minimal ARM for people that do not need a desktop"
meta_keywords = "manjaro minimal arm, manjaro download"
+++

The Minimal edition is Manjaro ARM, but without a Desktop Environment or Xorg at all.

Device Maintainer: Dan Johansen
Edition Maintainer: Dan Johansen

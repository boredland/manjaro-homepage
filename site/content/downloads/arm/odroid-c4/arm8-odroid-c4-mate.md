+++
Download_x64 = "https://github.com/manjaro-arm/oc4-images/releases/download/21.06/Manjaro-ARM-mate-oc4-21.06.img.xz"
Download_x64_Checksum = "035779c32622afcbbf60a02b759b5a67f12814f1"
Download_x64_Sig = "https://github.com/manjaro-arm/oc4-images/releases/download/21.06/Manjaro-ARM-mate-oc4-21.06.img.xz.sig"
Download_x64_Torrent = "https://github.com/manjaro-arm/oc4-images/releases/download/21.06/Manjaro-ARM-mate-oc4-21.06.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Odroid C4 MATE"
Screenshot = "arm-mate-full.png"
Youtube = ""
edition = "ARM"
device = "Odroid C4"
shortDescription = "Manjaro MATE on ARM is a modern looking DE that follows the Gnome2 paradigm."
Thumbnail = "arm-mate-full.png"
Version = "21.06"
date = "06.2021"
title = "Odroid C4 MATE"
type="download-edition"
weight = "1"
meta_description = "Manjaro MATE on ARM is a modern looking DE that follows the Gnome2 paradigm."
meta_keywords = "manjaro mate arm, manjaro download"
+++

For people who look for a traditional experience

This edition comes with Mate, a desktop environment that continues the legacy of traditional user experience while carefully improving and modernizing it when needed.

Device Maintainer: Dan Johansen
Edition Maintainer: Ray Sherwin

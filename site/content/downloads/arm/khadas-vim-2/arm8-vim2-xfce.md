+++
Download_x64 = "https://github.com/manjaro-arm/vim2-images/releases/download/21.06/Manjaro-ARM-xfce-vim2-21.06.img.xz"
Download_x64_Checksum = "f20158f7a61b185c62ddacb76b2ee7bc0f2e30f2"
Download_x64_Sig = "https://github.com/manjaro-arm/vim2-images/releases/download/21.06/Manjaro-ARM-xfce-vim2-21.06.img.xz.sig"
Download_x64_Torrent = "https://github.com/manjaro-arm/vim2-images/releases/download/21.06/Manjaro-ARM-xfce-vim2-21.06.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Khadas Vim 2 XFCE"
Screenshot = "arm-xfce-full.png"
Youtube = ""
edition = "ARM"
device = "Khadas Vim 2"
shortDescription = "XFCE on ARM is one of the fastest DE's available and the most stable."
Thumbnail = "arm-xfce-full.png"
Version = "21.06"
date = "06.2021"
title = "Khadas Vim 2 XFCE"
type="download-edition"
weight = "1"
meta_description = "Manjaro XFCE on ARM is one of the fastest DE's available and the most stable."
meta_keywords = "manjaro xfce arm, manjaro download"
+++

This edition is supported by the Manjaro ARM team and comes with XFCE desktop.

XFCE is a lightweight, and very stable, GTK based desktop. It's modular and very customizable.

Device Maintainer: Furkan Kardame
Edition Maintainer: Ray Sherwin

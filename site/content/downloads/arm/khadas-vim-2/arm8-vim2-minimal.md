+++
Download_x64 = "https://github.com/manjaro-arm/vim2-images/releases/download/21.06/Manjaro-ARM-minimal-vim2-21.06.img.xz"
Download_x64_Checksum = "527b0647ac5a7bc61816366baf5c8ba463ae0e86"
Download_x64_Sig = "https://github.com/manjaro-arm/vim2-images/releases/download/21.06/Manjaro-ARM-minimal-vim2-21.06.img.xz.sig"
Download_x64_Torrent = "https://github.com/manjaro-arm/vim2-images/releases/download/21.06/Manjaro-ARM-minimal-vim2-21.06.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Khadas Vim 2 Minimal"
Screenshot = "arm-minimal-full.png"
Youtube = ""
edition = "ARM"
device = "Khadas Vim 2"
shortDescription = "For people that do not need a desktop"
Thumbnail = "arm-minimal-full.png"
Version = "21.06"
date = "06.2021"
title = "Khadas Vim 2 Minimal"
type="download-edition"
weight = "1"
meta_description = "Manjaro arm for people that do not need a desktop"
meta_keywords = "manjaro minimal arm, manjaro download"
+++

The Minimal edition is Manjaro ARM, but without a Desktop Environment or Xorg at all.

Device Maintainer: Furkan Kardame
Edition Maintainer: Dan Johansen

+++
Download_x64 = "https://github.com/manjaro-arm/vim2-images/releases/download/21.06/Manjaro-ARM-sway-vim2-21.06.img.xz"
Download_x64_Checksum = "da9adad9ddd410010cef8b0c1362cb42b5c37384"
Download_x64_Sig = "https://github.com/manjaro-arm/vim2-images/releases/download/21.06/Manjaro-ARM-sway-vim2-21.06.img.xz.sig"
Download_x64_Torrent = "https://github.com/manjaro-arm/vim2-images/releases/download/21.06/Manjaro-ARM-sway-vim2-21.06.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Khadas Vim 2 Sway"
Screenshot = "arm-sway-full.png"
Youtube = ""
edition = "ARM"
device = "Khadas Vim 2"
shortDescription = "Manjaro ARM with the lightweight tiling window manager sway."
Thumbnail = "arm-sway-full.png"
Version = "21.06"
date = "06.2021"
title = "Khadas Vim 2 Sway"
type="download-edition"
weight = "1"
meta_description = "Manjaro sway on ARM is a lightweight tiling window manager for Wayland"
meta_keywords = "manjaro sway arm, manjaro download"
+++

This edition is supported by the Manjaro ARM team and comes with the sway tiling window manager.

Sway is an extremely lightweight tiling window manager for Wayland, famous for its efficiency with screen space and keyboard controlled workflow. The ideal environment for text and terminal focused usage for advanced users.

Device Maintainer: Furkan Kardame
Edition Maintainer: Andreas Gerlach

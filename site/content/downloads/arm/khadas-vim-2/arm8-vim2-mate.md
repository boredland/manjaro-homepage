+++
Download_x64 = "https://github.com/manjaro-arm/vim2-images/releases/download/21.06/Manjaro-ARM-mate-vim2-21.06.img.xz"
Download_x64_Checksum = "6ccc5eb0f85ea5bcfed632bdf7b22f3c5235d0fa"
Download_x64_Sig = "https://github.com/manjaro-arm/vim2-images/releases/download/21.06/Manjaro-ARM-mate-vim2-21.06.img.xz.sig"
Download_x64_Torrent = "https://github.com/manjaro-arm/vim2-images/releases/download/21.06/Manjaro-ARM-mate-vim2-21.06.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Khadas Vim 2 MATE"
Screenshot = "arm-mate-full.png"
Youtube = ""
edition = "ARM"
device = "Khadas Vim 2"
shortDescription = "Manjaro MATE on ARM is a modern looking DE that follows the Gnome2 paradigm."
Thumbnail = "arm-mate-full.png"
Version = "21.06"
date = "06.2021"
title = "Khadas Vim 2 MATE"
type="download-edition"
weight = "1"
meta_description = "Manjaro MATE on ARM is a modern looking DE that follows the Gnome2 paradigm."
meta_keywords = "manjaro mate arm, manjaro download"
+++

For people who look for a traditional experience

This edition comes with Mate, a desktop environment that continues the legacy of traditional user experience while carefully improving and modernizing it when needed.

Device Maintainer: Furkan Kardame
Edition Maintainer: Ray Sherwin

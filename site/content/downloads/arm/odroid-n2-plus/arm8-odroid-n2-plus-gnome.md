+++
Download_x64 = "https://github.com/manjaro-arm/on2-plus-images/releases/download/21.06/Manjaro-ARM-gnome-on2-plus-21.06.img.xz"
Download_x64_Checksum = "f61f35f6b1439739f6e1d3adb35f34228f760269"
Download_x64_Sig = "https://github.com/manjaro-arm/on2-plus-images/releases/download/21.06/Manjaro-ARM-gnome-on2-plus-21.06.img.xz.sig"
Download_x64_Torrent = "https://github.com/manjaro-arm/on2-plus-images/releases/download/21.06/Manjaro-ARM-gnome-on2-plus-21.06.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Odroid N2+ Gnome"
Screenshot = "arm-gnome-full.png"
Youtube = ""
edition = "ARM"
device = "Odroid N2+"
shortDescription = "Manjaro Gnome on ARM is a modern DE with a modern workflow."
Thumbnail = "arm-gnome-full.png"
Version = "21.06"
date = "06.2021"
title = "Odroid N2+ Gnome"
type="download-edition"
weight = "1"
meta_description = "Manjaro Gnome on ARM is a modern DE with a modern workflow."
meta_keywords = "manjaro gnome arm, manjaro download"
+++

For people who look for a modern user experience.

This edition comes with Gnome, a desktop based on GTK3 and GTK4, that has a slick user experience.

Device Maintainer: Dan Johansen
Edition Maintainer: Andreas Gerlach

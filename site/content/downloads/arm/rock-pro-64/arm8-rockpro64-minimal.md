+++
Download_x64 = "https://github.com/manjaro-arm/rockpro64-images/releases/download/21.06/Manjaro-ARM-minimal-rockpro64-21.06.img.xz"
Download_x64_Checksum = "8c9eb50a75b87c4fd4fd3584f246f6685b248f49"
Download_x64_Sig = "https://github.com/manjaro-arm/rockpro64-images/releases/download/21.06/Manjaro-ARM-minimal-rockpro64-21.06.img.xz.sig"
Download_x64_Torrent = "https://github.com/manjaro-arm/rockpro64-images/releases/download/21.06/Manjaro-ARM-minimal-rockpro64-21.06.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "RockPro64 Minimal"
Screenshot = "arm-minimal-full.png"
Youtube = ""
edition = "ARM"
device = "RockPro64"
shortDescription = "For people that do not need a desktop"
Thumbnail = "arm-minimal-full.png"
Version = "21.06"
date = "06.2021"
title = "RockPro64 Minimal"
type="download-edition"
weight = "1"
meta_description = "Manjaro minimal ARM for people that do not need a desktop"
meta_keywords = "manjaro minimal arm, manjaro download"
+++

The Minimal edition is Manjaro ARM, but without a Desktop Environment or Xorg at all.

Device Maintainer: Dan Johansen
Edition Maintainer: Dan Johansen

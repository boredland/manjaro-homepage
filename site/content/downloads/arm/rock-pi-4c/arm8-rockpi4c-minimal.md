+++
Download_x64 = "https://github.com/manjaro-arm/rockpi4c-images/releases/download/21.06/Manjaro-ARM-minimal-rockpi4c-21.06.img.xz"
Download_x64_Checksum = "2f9b8b8c43b7a6847905cd5ae963201e745db871"
Download_x64_Sig = "https://github.com/manjaro-arm/rockpi4c-images/releases/download/21.06/Manjaro-ARM-minimal-rockpi4c-21.06.img.xz.sig"
Download_x64_Torrent = "https://github.com/manjaro-arm/rockpi4c-images/releases/download/21.06/Manjaro-ARM-minimal-rockpi4c-21.06.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Rock Pi 4C Minimal"
Screenshot = "arm-minimal-full.png"
Youtube = ""
edition = "ARM"
device = "Rock Pi 4C"
shortDescription = "For people that do not need a desktop"
Thumbnail = "arm-minimal-full.png"
Version = "21.06"
date = "06.2021"
title = "Rock Pi 4C Minimal"
type="download-edition"
weight = "1"
meta_description = "Manjaro minimal ARM for people that do not need a desktop"
meta_keywords = "manjaro minimal arm, manjaro download"
+++

The Minimal edition is Manjaro ARM, but without a Desktop Environment or Xorg at all.

Device Maintainer: Dan Johansen
Edition Maintainer: Dan Johansen

+++
Download_x64 = "https://github.com/manjaro-arm/rockpi4c-images/releases/download/21.06/Manjaro-ARM-gnome-rockpi4c-21.06.img.xz"
Download_x64_Checksum = "1f0a05918014f13dc85bfabdef82b448974b7174"
Download_x64_Sig = "https://github.com/manjaro-arm/rockpi4c-images/releases/download/21.06/Manjaro-ARM-gnome-rockpi4c-21.06.img.xz.sig"
Download_x64_Torrent = "https://github.com/manjaro-arm/rockpi4c-images/releases/download/21.06/Manjaro-ARM-gnome-rockpi4c-21.06.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Rock Pi 4C Gnome"
Screenshot = "arm-gnome-full.png"
Youtube = ""
edition = "ARM"
device = "Rock Pi 4C"
shortDescription = "Manjaro Gnome on ARM is a modern DE with a modern workflow."
Thumbnail = "arm-gnome-full.png"
Version = "21.06"
date = "06.2021"
title = "Rock Pi 4C Gnome"
type="download-edition"
weight = "1"
meta_description = "Manjaro Gnome on ARM is a modern DE with a modern workflow."
meta_keywords = "manjaro gnome arm, manjaro download"
+++

For people who look for a modern user experience.

This edition comes with Gnome, a desktop based on GTK3 and GTK4, that has a slick user experience.

Device Maintainer: Dan Johansen
Edition Maintainer: Andreas Gerlach

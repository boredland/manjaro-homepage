+++
Download_x64 = "https://github.com/manjaro-arm/pine64-lts-images/releases/download/21.06/Manjaro-ARM-sway-pine64-lts-21.06.img.xz"
Download_x64_Checksum = "4352dc8411510179af68d5a7d8d662136fabe54f"
Download_x64_Sig = "https://github.com/manjaro-arm/pine64-lts-images/releases/download/21.06/Manjaro-ARM-sway-pine64-lts-21.06.img.xz.sig"
Download_x64_Torrent = "https://github.com/manjaro-arm/pine64-lts-images/releases/download/21.06/Manjaro-ARM-sway-pine64-lts-21.06.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Pine64 LTS Sway"
Screenshot = "arm-sway-full.png"
Youtube = ""
edition = "ARM"
device = "Pine64 LTS"
shortDescription = "Manjaro ARM with the lightweight tiling window manager sway."
Thumbnail = "arm-sway-full.png"
Version = "21.06"
date = "06.2021"
title = "Pine64 LTS Sway"
type="download-edition"
weight = "1"
meta_description = "Manjaro sway on ARM is a lightweight tiling window manager for Wayland"
meta_keywords = "manjaro sway arm, manjaro download"
+++

This edition is supported by the Manjaro ARM team and comes with the sway tiling window manager.

Sway is an extremely lightweight tiling window manager for Wayland, famous for its efficiency with screen space and keyboard controlled workflow. The ideal environment for text and terminal focused usage for advanced users.

Device Maintainer: Dan Johansen
Edition Maintainer: Andreas Gerlach

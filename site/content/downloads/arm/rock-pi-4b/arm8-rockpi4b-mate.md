+++
Download_x64 = "https://github.com/manjaro-arm/rockpi4b-images/releases/download/21.06/Manjaro-ARM-mate-rockpi4b-21.06.img.xz"
Download_x64_Checksum = "7a88e4af1a8312d590a9f578d599a4f74c757acc"
Download_x64_Sig = "https://github.com/manjaro-arm/rockpi4b-images/releases/download/21.06/Manjaro-ARM-mate-rockpi4b-21.06.img.xz.sig"
Download_x64_Torrent = "https://github.com/manjaro-arm/rockpi4b-images/releases/download/21.06/Manjaro-ARM-mate-rockpi4b-21.06.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Rock Pi 4B MATE"
Screenshot = "arm-mate-full.png"
Youtube = ""
edition = "ARM"
device = "Rock Pi 4B"
shortDescription = "Manjaro MATE on ARM is a modern looking DE that follows the Gnome2 paradigm."
Thumbnail = "arm-mate-full.png"
Version = "21.06"
date = "06.2021"
title = "Rock Pi 4B MATE"
type="download-edition"
weight = "1"
meta_description = "Manjaro MATE on ARM is a modern looking DE that follows the Gnome2 paradigm."
meta_keywords = "manjaro mate arm, manjaro download"
+++

For people who look for a traditional experience

This edition comes with Mate, a desktop environment that continues the legacy of traditional user experience while carefully improving and modernizing it when needed.

Device Maintainer: Dan Johansen
Edition Maintainer: Ray Sherwin

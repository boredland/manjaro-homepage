+++
Download_x64 = "https://github.com/manjaro-arm/rockpi4b-images/releases/download/21.06/Manjaro-ARM-minimal-rockpi4b-21.06.img.xz"
Download_x64_Checksum = "9fc5fb1e4b55b12cecbbeef1cd52b2ed9bff0a1b"
Download_x64_Sig = "https://github.com/manjaro-arm/rockpi4b-images/releases/download/21.06/Manjaro-ARM-minimal-rockpi4b-21.06.img.xz.sig"
Download_x64_Torrent = "https://github.com/manjaro-arm/rockpi4b-images/releases/download/21.06/Manjaro-ARM-minimal-rockpi4b-21.06.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Rock Pi 4B Minimal"
Screenshot = "arm-minimal-full.png"
Youtube = ""
edition = "ARM"
device = "Rock Pi 4B"
shortDescription = "For people that do not need a desktop"
Thumbnail = "arm-minimal-full.png"
Version = "21.06"
date = "06.2021"
title = "Rock Pi 4B Minimal"
type="download-edition"
weight = "1"
meta_description = "Manjaro minimal ARM for people that do not need a desktop"
meta_keywords = "manjaro minimal arm, manjaro download"
+++

This edition is supported by the Manjaro ARM team.

The Minimal edition is Manjaro ARM, but without a Desktop Environment or Xorg at all.

Device Maintainer: Dan Johansen
Edition Maintainer: Dan Johansen

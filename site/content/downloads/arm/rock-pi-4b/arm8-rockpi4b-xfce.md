+++
Download_x64 = "https://github.com/manjaro-arm/rockpi4b-images/releases/download/21.06/Manjaro-ARM-xfce-rockpi4b-21.06.img.xz"
Download_x64_Checksum = "b457f6767f10b7d14c68e63d53c6255de5af629b"
Download_x64_Sig = "https://github.com/manjaro-arm/rockpi4b-images/releases/download/21.06/Manjaro-ARM-xfce-rockpi4b-21.06.img.xz.sig"
Download_x64_Torrent = "https://github.com/manjaro-arm/rockpi4b-images/releases/download/21.06/Manjaro-ARM-xfce-rockpi4b-21.06.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Rock Pi 4B XFCE"
Screenshot = "arm-xfce-full.png"
Youtube = ""
edition = "ARM"
device = "Rock Pi 4B"
shortDescription = "XFCE on ARM is one of the fastest DE's available and the most stable."
Thumbnail = "arm-xfce-full.png"
Version = "21.06"
date = "06.2021"
title = "Rock Pi 4B XFCE"
type="download-edition"
weight = "1"
meta_description = "Manjaro XFCE on ARM is one of the fastest DE's available and the most stable."
meta_keywords = "manjaro xfce arm, manjaro download"
+++

This edition is supported by the Manjaro ARM team and comes with XFCE desktop.

XFCE is a lightweight, and very stable, GTK based desktop. It's modular and very customizable.

Device Maintainer: Dan Johansen
Edition Maintainer: Ray Sherwin

+++
Download_x64 = "https://github.com/manjaro-arm/pbpro-images/releases/download/21.06/Manjaro-ARM-mate-pbpro-21.06.img.xz"
Download_x64_Checksum = "e83e52a5f12bd8216f23b3df76b57d31b8a05280"
Download_x64_Sig = "https://github.com/manjaro-arm/pbpro-images/releases/download/21.06/Manjaro-ARM-mate-pbpro-21.06.img.xz.sig"
Download_x64_Torrent = "https://github.com/manjaro-arm/pbpro-images/releases/download/21.06/Manjaro-ARM-mate-pbpro-21.06.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Pinebook Pro MATE"
Screenshot = "arm-mate-full.png"
Youtube = ""
edition = "ARM"
device = "Pinebook Pro"
shortDescription = "Manjaro MATE on ARM is a modern looking DE that follows the Gnome2 paradigm."
Thumbnail = "arm-mate-full.png"
Version = "21.06"
date = "06.2021"
title = "Pinebook Pro MATE"
type="download-edition"
weight = "1"
meta_description = "Manjaro MATE on ARM is a modern looking DE that follows the Gnome2 paradigm."
meta_keywords = "manjaro mate arm, manjaro download"
+++

For people who look for a traditional experience

This edition comes with Mate, a desktop environment that continues the legacy of traditional user experience while carefully improving and modernizing it when needed.

Device Maintainer: Dan Johansen
Edition Maintainer: Ray Sherwin

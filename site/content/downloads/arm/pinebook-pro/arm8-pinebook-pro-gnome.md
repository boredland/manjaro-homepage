+++
Download_x64 = "https://github.com/manjaro-arm/pbpro-images/releases/download/21.06/Manjaro-ARM-gnome-pbpro-21.06.img.xz"
Download_x64_Checksum = "3df54bb5c15287f772476f713ee8d071848d2b25"
Download_x64_Sig = "https://github.com/manjaro-arm/pbpro-images/releases/download/21.06/Manjaro-ARM-gnome-pbpro-21.06.img.xz.sig"
Download_x64_Torrent = "https://github.com/manjaro-arm/pbpro-images/releases/download/21.06/Manjaro-ARM-gnome-pbpro-21.06.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Pinebook Pro Gnome"
Screenshot = "arm-gnome-full.png"
Youtube = ""
edition = "ARM"
device = "Pinebook Pro"
shortDescription = "Manjaro Gnome on ARM is a modern DE with a modern workflow."
Thumbnail = "arm-gnome-full.png"
Version = "21.06"
date = "06.2021"
title = "Pinebook Pro Gnome"
type="download-edition"
weight = "1"
meta_description = "Manjaro Gnome on ARM is a modern DE with a modern workflow."
meta_keywords = "manjaro gnome arm, manjaro download"
+++

For people who look for a modern user experience.

This edition comes with Gnome, a desktop based on GTK3 and GTK4, that has a slick user experience.

Device Maintainer: Dan Johansen
Edition Maintainer: Andreas Gerlach

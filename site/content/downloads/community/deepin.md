+++
Download_x64 = "https://download.manjaro.org/deepin/21.0.7/manjaro-deepin-21.0.7-210623-linux510.iso"
Download_x64_Checksum = "19c4d7aeb391864dfae06ab5c9991b74855d0eeb"
Download_x64_Sig = "https://download.manjaro.org/deepin/21.0.7/manjaro-deepin-21.0.7-210623-linux510.iso.sig"
Download_x64_Torrent = "https://download.manjaro.org/deepin/21.0.7/manjaro-deepin-21.0.7-210623-linux510.iso.torrent"
Download_Minimal_x64 = "https://download.manjaro.org/deepin/21.0.7/manjaro-deepin-21.0.7-minimal-210623-linux510.iso"
Download_Minimal_x64_Checksum = "7777535999edd1bfedff1b43c3570ddd4a43e5a3"
Download_Minimal_x64_Sig = "https://download.manjaro.org/deepin/21.0.7/manjaro-deepin-21.0.7-minimal-210623-linux510.iso.sig"
Download_Minimal_x64_Torrent = "https://download.manjaro.org/deepin/21.0.7/manjaro-deepin-21.0.7-minimal-210623-linux510.iso.torrent"
Name = "Deepin"
Screenshot = "deepin-full.jpg"
edition = "Community"
shortDescription = "Modern and elegant desktop"
Thumbnail = "deepin-full.jpg"
Version = "21.0.7"
date = "2021-06-24T12:56:36UTC"
title = "Deepin Stable"
type="download-edition"
weight = "5"
meta_description = "Manjaro deepin modern and elegant desktop"
meta_keywords = "manjaro deepin download, manjaro download"
+++

This edition is supported by the Manjaro community and comes with the [Deepin](https://www.deepin.org/) desktop that provides a very beginner-friendly and elegant experience."

If you are looking for older images check the [Deepin](https://osdn.net/projects/manjaro-archive/storage/deepin/) archive.



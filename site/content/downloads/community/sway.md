+++
redirect = "https://manjaro-sway.download/"
Download_x64 = ""
Download_x64_Checksum = ""
Download_x64_Sig = ""
Download_x64_Torrent = ""
Download_Minimal_x64 = ""
Download_Minimal_x64_Checksum = ""
Download_Minimal_x64_Sig = ""
Download_Minimal_x64_Torrent = ""
Name = "Sway"
Screenshot = "sway.jpg"
edition = "Community"
shortDescription = "For wayland fanatics and \u2328 keyboard enthusiasts"
Thumbnail = "sway.jpg"
Version = "Daily"
date = "2021-04-22T18:30:14UTC"
title = "Sway"
type="download-edition"
weight = "5"
meta_description = "Sway is a tiling Wayland compositor and a drop-in replacement for the i3 window manager."
meta_keywords = "Sway is a tiling Wayland compositor and a drop-in replacement for the i3 window manager."
+++

This edition is supported by the Manjaro community and comes with Sway. Sway is a tiling Wayland compositor based on wlroots.

Please refer to the [manjaro sway github](https://github.com/Manjaro-Sway/manjaro-sway) repository for [downloads](https://github.com/Manjaro-Sway/manjaro-sway/releases), [issues](https://github.com/Manjaro-Sway/manjaro-sway/issues), [frequently asked questions](https://github.com/Manjaro-Sway/manjaro-sway/blob/main/SUPPORT.md) and if you'd like to contribute.

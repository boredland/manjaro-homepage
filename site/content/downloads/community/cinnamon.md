+++
Download_x64 = "https://download.manjaro.org/cinnamon/21.0.7/manjaro-cinnamon-21.0.7-210623-linux510.iso"
Download_x64_Checksum = "5acbf40a6005ae17811d34835cdb32eaaf884233"
Download_x64_Sig = "https://download.manjaro.org/cinnamon/21.0.7/manjaro-cinnamon-21.0.7-210623-linux510.iso.sig"
Download_x64_Torrent = "https://download.manjaro.org/cinnamon/21.0.7/manjaro-cinnamon-21.0.7-210623-linux510.iso.torrent"
Download_Minimal_x64 = "https://download.manjaro.org/cinnamon/21.0.7/manjaro-cinnamon-21.0.7-minimal-210623-linux510.iso"
Download_Minimal_x64_Checksum = "9efdd4427813d86aa60dcf40fbfdd775cd57df3f"
Download_Minimal_x64_Sig = "https://download.manjaro.org/cinnamon/21.0.7/manjaro-cinnamon-21.0.7-minimal-210623-linux510.iso.sig"
Download_Minimal_x64_Torrent = "https://download.manjaro.org/cinnamon/21.0.7/manjaro-cinnamon-21.0.7-minimal-210623-linux510.iso.torrent"
Name = "Cinnamon"
Screenshot = "cinnamon-full.jpg"
edition = "Community"
shortDescription = "For people who look for a traditional desktop with modern technology"
Thumbnail = "cinnamon-full.jpg"
Version = "21.0.7"
date = "2021-06-24T12:56:36UTC"
title = "Cinnamon"
type="download-edition"
weight = "5"
meta_description = "Manjaro cinnamon for people who look for a traditional desktop with modern technology"
meta_keywords = "manjaro cinnamon download, manjaro download"
+++

This edition is supported by the Manjaro community and comes with Cinnamon, a desktop based on modern technology that keeps known and proven concepts.

If you are looking for older images check the [Cinnamon](https://osdn.net/projects/manjaro-archive/storage/cinnamon/) archive.

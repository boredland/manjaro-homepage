+++
Download_x64 = "https://download.manjaro.org/budgie/21.0.7/manjaro-budgie-21.0.7-210623-linux510.iso"
Download_x64_Checksum = "ff5cf8aa492d0ccf25bbcd9cf8a142cc5a490c93"
Download_x64_Sig = "https://download.manjaro.org/budgie/21.0.7/manjaro-budgie-21.0.7-210623-linux510.iso.sig"
Download_x64_Torrent = "https://download.manjaro.org/budgie/21.0.7/manjaro-budgie-21.0.7-210623-linux510.iso.torrent"
Download_Minimal_x64 = "https://download.manjaro.org/budgie/21.0.7/manjaro-budgie-21.0.7-minimal-210623-linux510.iso"
Download_Minimal_x64_Checksum = "6e2e6f61c860d5d4c6468cd7544ce1d70e0e25e1"
Download_Minimal_x64_Sig = "https://download.manjaro.org/budgie/21.0.7/manjaro-budgie-21.0.7-minimal-210623-linux510.iso.sig"
Download_Minimal_x64_Torrent = "https://download.manjaro.org/budgie/21.0.7/manjaro-budgie-21.0.7-minimal-210623-linux510.iso.torrent"
Name = "Budgie"
Screenshot = "budgie-full.jpg"
edition = "Community"
shortDescription = "For people who want a simple and elegant desktop"
Thumbnail = "budgie-full.jpg"
Version = "21.0.7"
date = "2021-06-24T18:30:14UTC"
title = "Budgie"
type="download-edition"
weight = "5"
meta_description = "Manjaro budgie for people who want a simple and elegant desktop"
meta_keywords = "manjaro budgie download, manjaro download"
+++

This edition is supported by the Manjaro community and comes with Budgie, the desktop of the [Solus project](https://getsol.us/). Budgie focuses on providing a simple-to-use and elegant desktop that fulfills the needs of a modern user.

If you are looking for older images check the [Budgie](https://osdn.net/projects/manjaro-archive/storage/budgie/) archive.



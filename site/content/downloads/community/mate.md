+++
Download_x64 = "https://download.manjaro.org/mate/21.0.7/manjaro-mate-21.0.7-210623-linux510.iso"
Download_x64_Checksum = "7d2ea63a3dfc6c38042eefb4f2c1e71b197f782f"
Download_x64_Sig = "https://download.manjaro.org/mate/21.0.7/manjaro-mate-21.0.7-210623-linux510.iso.sig"
Download_x64_Torrent = "https://download.manjaro.org/mate/21.0.7/manjaro-mate-21.0.7-210623-linux510.iso.torrent"
Download_Minimal_x64 = "https://download.manjaro.org/mate/21.0.7/manjaro-mate-21.0.7-minimal-210623-linux510.iso"
Download_Minimal_x64_Checksum = "586d53752af809908892caf91d77e9a437ffba57"
Download_Minimal_x64_Sig = "https://download.manjaro.org/mate/21.0.7/manjaro-mate-21.0.7-minimal-210623-linux510.iso"
Download_Minimal_x64_Torrent = "https://download.manjaro.org/mate/21.0.7/manjaro-mate-21.0.7-minimal-210623-linux510.iso.torrent"
Name = "MATE"
Screenshot = "mate-full.jpg"
edition = "Community"
shortDescription = "For people who look for a traditional experience"
Thumbnail = "mate-full.jpg"
Version = "21.0.7"
date = "2020-06-24T20:23:00UTC"
title = "Mate"
type="download-edition"
weight = "5"
meta_description = "Manjaro mate for people who look for a traditional experience"
meta_keywords = "manjaro mate download, manjaro download"
+++

This edition is supported by the Manjaro community and comes with Mate, a desktop environment that continues the legacy of traditional user experience while carefully improving and modernizing it when needed.

If you are looking for older images check the [Mate](https://osdn.net/projects/manjaro-archive/storage/mate/) archive.


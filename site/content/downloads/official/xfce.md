+++
Download_x64 = "https://download.manjaro.org/xfce/21.1.0/manjaro-xfce-21.1.0-210817-linux513.iso"
Download_x64_Checksum = "a6c56c3cba42a3ccb976acacde253673e27f65e5"
Download_x64_Sig = "https://download.manjaro.org/xfce/21.1.0/manjaro-xfce-21.1.0-210817-linux513.iso.sig"
Download_x64_Torrent = "https://download.manjaro.org/xfce/21.1.0/manjaro-xfce-21.1.0-210817-linux513.iso.torrent"

Download_Minimal_x64 = "https://download.manjaro.org/xfce/21.1.0/manjaro-xfce-21.1.0-minimal-210817-linux513.iso"
Download_Minimal_x64_Checksum = "9b7055cba2487cc410f6238fd6de30091aefce6c"
Download_Minimal_x64_Sig = "https://download.manjaro.org/xfce/21.1.0/manjaro-xfce-21.1.0-minimal-210817-linux513.iso.sig"
Download_Minimal_x64_Torrent = "https://download.manjaro.org/xfce/21.1.0/manjaro-xfce-21.1.0-minimal-210817-linux513.iso.torrent"

Download_Minimal_lts = "https://download.manjaro.org/xfce/21.1.0/manjaro-xfce-21.1.0-minimal-210817-linux54.iso"
Download_Minimal_x64_Checksum_lts = "6f274c960d83d24e05c0d90ac5b8999e308b249b"
Download_Minimal_x64_Sig_lts = "https://download.manjaro.org/xfce/21.1.0/manjaro-xfce-21.1.0-minimal-210817-linux54.iso.sig"
Download_Minimal_x64_Torrent_lts = "https://download.manjaro.org/xfce/21.1.0/manjaro-xfce-21.1.0-minimal-210817-linux54.iso.torrent"

Name = "XFCE"
Screenshot = "xfce-full.jpg"
Youtube = "I6WgT7JkkQk"
edition = "Official"
shortDescription = "For people who want a reliable and fast desktop"
Thumbnail = "xfce.jpg"
Version = "21.1.0"
date = "2021-08-17T05:38:06UTC"
title = "XFCE"
type="download-edition"
weight = "1"
meta_description = "Manjaro xfce for people who want a reliable and fast desktop"
meta_keywords = "manjaro xfce download, manjaro download"
+++

This edition is supported by the Manjaro team and comes with XFCE, a reliable desktop with high configurability.

Xfce is a lightweight desktop environment for UNIX-like operating systems. It aims to be fast and low on system resources, while still being visually appealing and user friendly. Xfce embodies the traditional UNIX philosophy of modularity and re-usability. It consists of a number of components that provide the full functionality one can expect of a modern desktop environment. They are packaged separately and you can pick among the available packages to create the optimal personal working environment.

If you are looking for older images check the [XFCE](https://osdn.net/projects/manjaro-archive/storage/xfce/) archive.



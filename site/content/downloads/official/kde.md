+++
Download_x64 = "https://download.manjaro.org/kde/21.1.0/manjaro-kde-21.1.0-210817-linux513.iso"
Download_x64_Checksum = "4d18f0b445434af814974b4942c80ee8f3266efe"
Download_x64_Sig = "https://download.manjaro.org/kde/21.1.0/manjaro-kde-21.1.0-210817-linux513.iso.sig"
Download_x64_Torrent = "https://download.manjaro.org/kde/21.1.0/manjaro-kde-21.1.0-210817-linux513.iso.torrent"

Download_Minimal_x64 = "https://download.manjaro.org/kde/21.1.0/manjaro-kde-21.1.0-minimal-210817-linux513.iso"
Download_Minimal_x64_Checksum = "4c7ead0e23aaeb18b31a222557dd53905d04deb8"
Download_Minimal_x64_Sig = "https://download.manjaro.org/kde/21.1.0/manjaro-kde-21.1.0-minimal-210817-linux513.iso.sig"
Download_Minimal_x64_Torrent = "https://download.manjaro.org/kde/21.1.0/manjaro-kde-21.1.0-minimal-210817-linux513.iso.torrent"

Download_Minimal_lts = "https://download.manjaro.org/kde/21.1.0/manjaro-kde-21.1.0-minimal-210817-linux54.iso"
Download_Minimal_x64_Checksum_lts = "73cb75fd2a6f9cf20521f78e3549ba66bef10c7e"
Download_Minimal_x64_Sig_lts = "https://download.manjaro.org/kde/21.1.0/manjaro-kde-21.1.0-minimal-210817-linux54.iso.sig"
Download_Minimal_x64_Torrent_lts = "https://download.manjaro.org/kde/21.1.0/manjaro-kde-21.1.0-minimal-210817-linux54.iso.torrent"

Name = "KDE Plasma"
Screenshot = "kde-full.jpg"
Youtube = "31hQNvha9v8"
edition = "Official"
shortDescription = "Built-in interface to easily access and install themes, widgets, etc. While very user-friendly and certainly flashy."
Thumbnail = "kde.jpg"
Version = "21.1.0"
date = "2021-08-17T05:38:06UTC"
title = "KDE Plasma"
type="download-edition"
weight = "2"
meta_description = "Manjaro kde built-in interface to easily access and install themes, widgets, etc. While very user-friendly and certainly flashy."
meta_keywords = "manjaro kde download, manjaro download"
+++

This edition is supported by the Manjaro team and comes with KDE Plasma, a very modern and flexible desktop.

KDE Plasma is for people who want a user-friendly and customizable desktop. It is a feature-rich and versatile desktop environment that provides several different styles of menu to access applications. An excellent built-in interface to easily access and install new themes, widgets, etc, from the internet is also worth mentioning. KDE Plasma is simple by default, a clean work area for real-world usage which intends to stay out of your way, so the user is enabled to create the workflow that makes it more effective to complete tasks.

If you are looking for older images check the [KDE](https://osdn.net/projects/manjaro-archive/storage/kde/) archive.

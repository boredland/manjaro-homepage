+++
Download_x64 = "https://download.manjaro.org/gnome/21.1.0/manjaro-gnome-21.1.0-210817-linux513.iso"
Download_x64_Checksum = "47add780e6c9dd65e7f0f04beba086f4fa289a4c"
Download_x64_Sig = "https://download.manjaro.org/gnome/21.1.0/manjaro-gnome-21.1.0-210817-linux513.iso.sig"
Download_x64_Torrent = "https://download.manjaro.org/gnome/21.1.0/manjaro-gnome-21.1.0-210817-linux513.iso.torrent"

Download_Minimal_x64 = "https://download.manjaro.org/gnome/21.1.0/manjaro-gnome-21.1.0-minimal-210817-linux513.iso"
Download_Minimal_x64_Checksum = "58523836cd867b6d564c7058680a0ae9b37bb860"
Download_Minimal_x64_Sig = "https://download.manjaro.org/gnome/21.1.0/manjaro-gnome-21.1.0-minimal-210817-linux513.iso.sig"
Download_Minimal_x64_Torrent = "https://download.manjaro.org/gnome/21.1.0/manjaro-gnome-21.1.0-minimal-210817-linux513.iso.torrent"

Download_Minimal_lts = "https://download.manjaro.org/gnome/21.1.0/manjaro-gnome-21.1.0-minimal-210817-linux54.iso"
Download_Minimal_x64_Checksum_lts = "63a0a67edaed1a30c9e693eefeea765d0e6f62f0"
Download_Minimal_x64_Sig_lts = "https://download.manjaro.org/gnome/21.1.0/manjaro-gnome-21.1.0-minimal-210817-linux54.iso.sig"
Download_Minimal_x64_Torrent_lts = "https://download.manjaro.org/gnome/21.1.0/manjaro-gnome-21.1.0-minimal-210817-linux54.iso.torrent"

Name = "GNOME"
Screenshot = "gnome-full.jpg"
Youtube = "N1xem3UdgB8"
edition = "Official"
shortDescription = "For people who want a very modern and simple desktop"
Thumbnail = "gnome.jpg"
Version = "21.1.0"
date = "2021-08-17T05:38:06UTC"
title = "Gnome"
type="download-edition"
weight = "2"
meta_description = "Manjaro gnome for people who want a very modern and simple desktop"
meta_keywords = "manjaro gnome download, manjaro download"
+++

This edition is supported by the Manjaro team and comes with the GNOME 3 desktop that breaks with traditional concepts and allows users to focus on their tasks. Desktop-specific applications are crafted with care and clearly defined by guidelines that make them more consistent to use.

If you are looking for older images check the [GNOME](https://osdn.net/projects/manjaro-archive/storage/gnome/) archive.

+++ 
class = "landing-page" 
background = "/img/bg10.jpg" 
title =  "FREE OPERATING SYSTEM FOR EVERYONE" 
whatIsManjaro = "Manjaro is a professionally made operating system that is a suitable replacement for Windows or MacOS. Multiple Desktop Environments are available through our Official and Community editions." 
description = "description of the page for search engine meta tags"
facebook = "https://www.facebook.com/ManjaroLinux"
twitter = "https://twitter.com/ManjaroLinux"
youtube = "https://www.youtube.com/channel/UCdGFLV7h9RGeTUX7wa5rqGw"
gitlab = "https://gitlab.manjaro.org"
+++



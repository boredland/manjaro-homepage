+++ 
class = "landing-page" 
title = "Manjaro"
type = "story-overview"
+++ 


Is an accessible, friendly, open-source operating system. Providing all the benefits of cutting-edge software combined with a focus on getting started quickly, automated tools to require less manual intervention, and help readily available when needed. Manjaro is suitable for both newcomers and experienced computer users.


Unlike proprietary operating systems you have full control over your hardware without restrictions. This makes Manjaro a powerful Operating System ideal in home, work, and development environments.

It is easily possible to run many popular Windows applications, using compatibility software such as [Wine](https://www.winehq.org/), [PlayonLinux](https://www.playonlinux.com/) or [Proton](https://www.protondb.com/) via [Steam](https://store.steampowered.com/about/). The examples given here are far from comprehensive!

Representing a perfect middle-ground for those who want good performance, full control, and cutting-edge software but also a degree of software stability.
érightside
### <i class="fa fa-dollar-sign"></i> Free is better

Manjaro will always be completely free. We create it, so we can have a operating system that is easy to use and stable. You the user, are the main focus, we do not take control away from you and respect your privacy.

### <i class="fa fa-download"></i> Install Anything

There are thousands of software applications available in the software center, including fully compatible equivalents of popular Windows software such as MS Office. Any additional software is also completely free. Searching for applications to install on the internet is not necessary.

### <i class="fa fa-users"></i>Great Community

We have a polite, friendly and cheerful [Forum](https://forum.manjaro.org/), where everyone is welcoming and supportive. The forum is the right place to share knowledge and talk Linux with the community we all <i class="fa fa-heart"></i> it.

### <i class="fas fa-compact-disc"></i>Availability

Manjaro is available for 64-Bit architectures. [XFCE](https://www.xfce.org/), [KDE](https://kde.org/) and [Gnome](https://www.gnome.org/gnome-3/) [editions](/download/) are officially supported. Other flavors are maintained by the community.

ARM editions are specifically available for certain devices. Pre-built images can be downloaded.